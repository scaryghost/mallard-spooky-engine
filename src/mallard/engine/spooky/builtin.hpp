#pragma once

#include "environment.hpp"
#include "result.hpp"

#include <string>
#include <unordered_map>
#include <utility>
#include <vector>

namespace mallard {
namespace engine {
namespace spooky {

struct FunctionKey {
    std::vector<std::string> parameter_types;
    std::string name;
};

bool operator==(const FunctionKey& left, const FunctionKey& right);

}
}
}

namespace std {

template<>
struct hash<mallard::engine::spooky::FunctionKey> {
    size_t operator()(const mallard::engine::spooky::FunctionKey &k) const;
};

}


namespace mallard {
namespace engine {
namespace spooky {

extern const std::unordered_map<FunctionKey, Result(*)(const std::shared_ptr<Environment>& ...)> BUILTIN_FUNCTIONS;

}
}
}
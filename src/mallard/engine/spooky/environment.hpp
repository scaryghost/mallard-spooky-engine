#pragma once

#include "lazy_result.hpp"
#include "memory_pool.hpp"
#include "result.hpp"

#include "mallard/isa/instruction.hpp"

#include <cstdint>
#include <memory>
#include <string>
#include <unordered_map>

namespace mallard {
namespace engine {
namespace spooky {

struct Environment {
    static Environment& global();

    ~Environment();

    void persist_result(const std::string& variable_name, const Result& result);
    const Result& find_variable(const std::string& variable_name) const;

    MemoryPool* memory_pool;
    std::unordered_map<std::string, Result> variables;
    std::unordered_map<std::string, LazyResult> pending_variables;
    std::shared_ptr<Environment> parent;
};

}
}
}
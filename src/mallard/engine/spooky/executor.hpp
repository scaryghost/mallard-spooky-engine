#pragma once

#include <memory>

#include "environment.hpp"
#include "memory_pool.hpp"
#include "result.hpp"

#include "mallard/isa/instruction.hpp"

namespace mallard {
namespace engine {
namespace spooky {

Result execute_instruction(const std::shared_ptr<Environment>& env, const isa::Instruction& instruction);

Result execute_anonymous_function(const std::shared_ptr<Environment>& env, const isa::Instruction& instruction);
Result execute_binary_expression(const std::shared_ptr<Environment>& env, const isa::Instruction& instruction);
Result execute_int_literal(const std::shared_ptr<Environment>& env, const isa::Instruction& instruction);
Result execute_function_application(const std::shared_ptr<Environment>& env, const isa::Instruction& instruction);
Result execute_list_literal(const std::shared_ptr<Environment>& env, const isa::Instruction& instruction);
Result execute_variable(const std::shared_ptr<Environment>& env, const isa::Instruction& instruction);

}
}
}
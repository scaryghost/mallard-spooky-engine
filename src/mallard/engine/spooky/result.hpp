#pragma once

#include "mallard/isa/type.h"

#include <memory>
#include <string>

namespace mallard {
namespace engine {
namespace spooky {

struct Result {
    std::shared_ptr<void> value;
    MallardType type;
    std::string type_name;

    template <class T>
    std::shared_ptr<T> as() const {
        return std::static_pointer_cast<T>(value);
    }
};

}
}
}
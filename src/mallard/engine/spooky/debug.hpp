#pragma once

#include "mallard/isa/instruction.hpp"

#include <ostream>

namespace mallard {
namespace engine {
namespace spooky {

std::ostream& operator <<(std::ostream& os, const isa::Instruction& instruction);

}
}
}
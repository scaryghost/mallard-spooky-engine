#pragma once

#include "mallard/isa/instruction.hpp"

#include <string>

namespace mallard {
namespace engine {
namespace spooky {
namespace type {

struct Function {
    static const std::string& name();

    const isa::Instruction function_definition;
};

}
}
}
}
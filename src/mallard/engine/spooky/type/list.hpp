#pragma once

#include "mallard/engine/spooky/environment.hpp"
#include "mallard/engine/spooky/lazy_result.hpp"
#include "mallard/engine/spooky/result.hpp"

#include <string>
#include <vector>
#include <unordered_map>

namespace mallard {
namespace engine {
namespace spooky {
namespace type {

struct List {
    List(std::size_t capacity);
    List(const List& original);
    ~List();

    std::vector<Result> elements;
    std::unordered_map<std::size_t, LazyResult> pending;

    const Result& operator[](std::size_t i);
    void eval_all();

    static const std::string& name();

    static Result concatenate(const std::shared_ptr<Environment>& env, ...);
    static Result subscript(const std::shared_ptr<Environment>& env, ...);
};

}
}
}
}
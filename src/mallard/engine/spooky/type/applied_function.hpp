#pragma once

#include "mallard/isa/instruction.hpp"
#include "mallard/engine/spooky/environment.hpp"
#include "mallard/engine/spooky/result.hpp"

#include <cstddef>
#include <string>
#include <vector>

namespace mallard {
namespace engine {
namespace spooky {
namespace type {

struct AppliedFunction {
    static const std::string& name();

    static Result copy(const AppliedFunction& original);
    static Result create(const std::shared_ptr<Environment>& env, const isa::Instruction& function_definition);

    ~AppliedFunction();

    Result bind(const std::shared_ptr<Environment>& env, const std::vector<isa::Instruction*>& parameters);
    bool all_bound() const {
        return n_bound_variables == function_definition.literals.size();
    }

    std::shared_ptr<Environment> env;
    const isa::Instruction function_definition;
    std::size_t n_bound_variables;
};

}
}
}
}
#pragma once

#include "mallard/engine/spooky/environment.hpp"
#include "mallard/engine/spooky/result.hpp"

#include <cstdint>
#include <string>

namespace mallard {
namespace engine {
namespace spooky {
namespace type {

struct Integer {
    static const std::string& name();

    static Result wrap(MemoryPool* memory_pool, std::int32_t value);

    static Result add(const std::shared_ptr<Environment>& env, ...);
    static Result sub(const std::shared_ptr<Environment>& env, ...);
    static Result mul(const std::shared_ptr<Environment>& env, ...);
    static Result div(const std::shared_ptr<Environment>& env, ...);
    static Result mod(const std::shared_ptr<Environment>& env, ...);

    static Result eq(const std::shared_ptr<Environment>& env, ...);
    static Result neq(const std::shared_ptr<Environment>& env, ...);
    static Result lt(const std::shared_ptr<Environment>& env, ...);
    static Result lte(const std::shared_ptr<Environment>& env, ...);
    static Result gt(const std::shared_ptr<Environment>& env, ...);
    static Result gte(const std::shared_ptr<Environment>& env, ...);
};

}
}
}
}
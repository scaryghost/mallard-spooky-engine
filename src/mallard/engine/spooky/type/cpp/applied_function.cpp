#include "mallard/engine/spooky/exception.hpp"
#include "mallard/engine/spooky/type/applied_function.hpp"

#include <sstream>

using namespace std;

namespace mallard {
namespace engine {
namespace spooky {
namespace type {

static Result wrap(const shared_ptr<Environment>& env, const isa::Instruction& function_definition, size_t n_bound_variables) {
    auto memory_pool = env->memory_pool;
    void *addr = memory_pool->allocate(sizeof(type::AppliedFunction));
    auto value = new(addr) AppliedFunction {
        .env = make_shared<Environment>(
            Environment {
                .memory_pool = memory_pool,
                .parent = env
            }
        ),
        .function_definition = function_definition,
        .n_bound_variables = n_bound_variables
    };

    return Result {
        shared_ptr<void>(value, [memory_pool](auto p) {
            static_cast<type::AppliedFunction*>(p)->~AppliedFunction();
            memory_pool->free(p);
        }),
        .type_name = type::AppliedFunction::name()
    };
}

Result AppliedFunction::copy(const AppliedFunction& original) {
    return wrap(original.env, original.function_definition, original.n_bound_variables);
}

Result AppliedFunction::create(const shared_ptr<Environment>& env, const isa::Instruction& function_definition) {
    return wrap(env, function_definition, 0);
}

const string& AppliedFunction::name() {
    static string value = "AppliedFunction";
    return value;
}

AppliedFunction::~AppliedFunction() {
    env.reset();
}

Result AppliedFunction::bind(const shared_ptr<Environment>& env, const vector<isa::Instruction*>& parameters) {
    if (parameters.size() + n_bound_variables > function_definition.literals.size()) {
        stringstream msg;

        msg << "function only has " << function_definition.literals.size() - n_bound_variables 
            << " parameters but was given " << parameters.size() << " values";

        throw Exception(msg.str());
    }

    auto result = copy(*this);
    auto fn = result.as<type::AppliedFunction>();

    for(size_t i = 0; i < parameters.size(); i++) {
        auto name = function_definition.literals[i + n_bound_variables];

        fn->env->pending_variables.emplace(
            name, 
            LazyResult {
                .env = env, 
                .instruction = *(parameters[i])
            }
        );
    }
    fn->n_bound_variables += parameters.size();

    return result;
}

}
}
}
}
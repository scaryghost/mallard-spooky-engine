#include "mallard/engine/spooky/executor.hpp"
#include "mallard/engine/spooky/type/list.hpp"

#include <cstdarg>
#include <tuple>
#include <utility>

using namespace std;

namespace mallard {
namespace engine {
namespace spooky {
namespace type {

const string& List::name() {
    static string name = "function";
    return name;
}

List::List(size_t capacity) :
    elements(capacity)
{

}

List::List(const List& original) :
    elements(original.elements.begin(), original.elements.end()),
    pending(original.pending.begin(), original.pending.end())
{
        
}

List::~List() {
    for(auto& it: elements) {
        it.value.reset();
    }

    elements.clear();
    pending.clear();
}

const Result& List::operator[](std::size_t i) {
    auto it = pending.find(i);

    if (it != pending.end()) {
        elements[i] = execute_instruction(it->second.env, it->second.instruction);

        pending.erase(it);
    }

    return elements.at(i);
}

Result List::concatenate(const std::shared_ptr<Environment>& env, ...) {
    va_list args;
    va_start(args, env);

    auto src1 = va_arg(args, List*);
    auto src2 = va_arg(args, List*);

    auto memory_pool = env->memory_pool;
    auto addr = memory_pool->allocate(sizeof(type::List));
    auto new_list = new(addr) List(*src1);

    for(auto& it: src2->elements) {
        new_list->elements.push_back(it);
    }
    for(auto& it: src2->pending) {
        new_list->pending.insert({ it.first + src1->elements.size(), it.second });
    }
    
    return Result {
        shared_ptr<void>(new_list, [memory_pool](auto p) {
            static_cast<type::List*>(p)->~List();
            memory_pool->free(p);
        }),
        .type_name = type::List::name()
    };
}

Result List::subscript(const std::shared_ptr<Environment>& env, ...) {
    va_list args;
    va_start(args, env);

    auto src1 = va_arg(args, List*);
    auto src2 = va_arg(args, int32_t*);
    
    return (*src1)[*src2];
}

}
}
}
}
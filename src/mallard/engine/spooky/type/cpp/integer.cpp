#include "mallard/engine/spooky/stdlib/boolean.hpp"
#include "mallard/engine/spooky/type/integer.hpp"

#include <cstdarg>

using namespace std;

namespace mallard {
namespace engine {
namespace spooky {
namespace type {



#define POPULATE_INT_OP(name, op) Result Integer::name(const std::shared_ptr<Environment>& env, ...) {\
    va_list args;\
    va_start(args, env);\
\
    auto src1 = va_arg(args, int32_t*);\
    auto src2 = va_arg(args, int32_t*);\
\
    return wrap(env->memory_pool, *src1 op *src2);\
}

#define POPULATE_BOOL_OP(name, op) Result Integer::name(const std::shared_ptr<Environment>& env, ...) {\
    va_list args;\
    va_start(args, env);\
\
    auto src1 = va_arg(args, int32_t*);\
    auto src2 = va_arg(args, int32_t*);\
\
    return stdlib::BOOLEANS[*src1 op *src2];\
}


const string& Integer::name() {
    static string name = "i32";
    return name;
}

Result Integer::wrap(MemoryPool* memory_pool, int32_t value) {
    void* addr = memory_pool->allocate(4);
    *((int32_t*) addr) = value;

    return Result {
        shared_ptr<void>(addr, [memory_pool](auto p) {
            memory_pool->free(p);
        }),
        MT_INT,
        Integer::name()
    };
}

POPULATE_INT_OP(add, +)
POPULATE_INT_OP(sub, -)
POPULATE_INT_OP(mul, *)
POPULATE_INT_OP(div, /)
POPULATE_INT_OP(mod, %)

POPULATE_BOOL_OP(eq, ==)
POPULATE_BOOL_OP(neq, !=)
POPULATE_BOOL_OP(lt, <)
POPULATE_BOOL_OP(lte, <=)
POPULATE_BOOL_OP(gt, >)
POPULATE_BOOL_OP(gte, >=)

}
}
}
}
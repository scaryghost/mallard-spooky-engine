#include "mallard/engine/spooky/type/function.hpp"

using namespace std;

namespace mallard {
namespace engine {
namespace spooky {
namespace type {

const string& Function::name() {
    static string value = "function";
    return value;
}

}
}
}
}
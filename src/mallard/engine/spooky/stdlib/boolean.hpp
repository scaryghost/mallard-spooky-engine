#pragma once

#include "mallard/engine/spooky/result.hpp"

#include <array>

namespace mallard {
namespace engine {
namespace spooky {
namespace stdlib {

extern std::array<Result, 2> BOOLEANS;
extern Result* TRUE;
extern Result* FALSE;

}
}
}
}
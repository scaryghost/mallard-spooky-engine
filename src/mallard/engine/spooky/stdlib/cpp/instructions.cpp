#include "instructions.hpp"

namespace mallard {
namespace engine {
namespace spooky {
namespace stdlib {

isa::Instruction VAR_X = {
    .literals = { "x" },
    .opcode = isa::OpCode::VARIABLE
};

isa::Instruction VAR_Y = {
    .literals = { "y" },
    .opcode = isa::OpCode::VARIABLE
};

}
}
}
}
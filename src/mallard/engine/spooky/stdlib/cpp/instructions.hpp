#pragma once

#include "mallard/isa/instruction.hpp"

namespace mallard {
namespace engine {
namespace spooky {
namespace stdlib {

extern isa::Instruction VAR_X;
extern isa::Instruction VAR_Y;

}
}
}
}
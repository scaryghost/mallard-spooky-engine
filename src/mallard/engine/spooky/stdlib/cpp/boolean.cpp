#include "mallard/engine/spooky/stdlib/boolean.hpp"
#include "mallard/engine/spooky/type/function.hpp"

#include "instructions.hpp"

using namespace std;

namespace mallard {
namespace engine {
namespace spooky {
namespace stdlib {

static auto FN_TRUE = type::Function {
    .function_definition = isa::Instruction {
        .literals = { "x", "y" },
        .productions = { &VAR_X },
        .opcode = isa::OpCode::ANONYMOUS_FUNCTION
    }
};

static auto FN_FALSE = type::Function {
    .function_definition = isa::Instruction {
        .literals = { "x", "y" },
        .productions = { &VAR_Y },
        .opcode = isa::OpCode::ANONYMOUS_FUNCTION
    }
};

array<Result, 2> BOOLEANS = {
    Result {
        .value = shared_ptr<void>(shared_ptr<void>{} , &FN_FALSE),
        .type_name = type::Function::name()
    },
    Result {
        .value = shared_ptr<void>(shared_ptr<void>{} , &FN_TRUE),
        .type_name = type::Function::name()
    }
};

Result* TRUE = &BOOLEANS[1];
Result* FALSE = &BOOLEANS[0];

}
}
}
}
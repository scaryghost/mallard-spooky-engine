#include "mallard/engine/spooky/stdlib/control.hpp"
#include "mallard/engine/spooky/type/function.hpp"

#include "instructions.hpp"

using namespace std;

namespace mallard {
namespace engine {
namespace spooky {
namespace stdlib {

static isa::Instruction VAR_P = {
    .literals = { "p" },
    .opcode = isa::OpCode::VARIABLE
};

static isa::Instruction VAR_F = {
    .literals = { "f" },
    .opcode = isa::OpCode::VARIABLE
};

static isa::Instruction APPLY_X_Y_TO_P = {
    .productions = { &VAR_P, &VAR_X, &VAR_Y },
    .opcode = isa::OpCode::FUNCTION_APPLICATION
};

static isa::Instruction APPLY_X_TO_X = {
    .productions = { &VAR_X, &VAR_X },
    .opcode = isa::OpCode::FUNCTION_APPLICATION
};

static isa::Instruction APPLY_XX_TO_F = {
    .productions = { &VAR_F, &APPLY_X_TO_X },
    .opcode = isa::OpCode::FUNCTION_APPLICATION
};

// \x -> f(x x)
static isa::Instruction FN_Y_INNER_1 = {
    .literals = { "x" },
    .productions = { &APPLY_XX_TO_F },
    .opcode = isa::OpCode::ANONYMOUS_FUNCTION
};

// (\x -> f(x(x)))(\x -> f(x(x)))
static isa::Instruction FN_Y_INNER_2 = {
    .productions = { &FN_Y_INNER_1, &FN_Y_INNER_1 },
    .opcode = isa::OpCode::FUNCTION_APPLICATION
};

static type::Function FN_IF = type::Function {
    .function_definition = isa::Instruction {
        .literals = { "p", "x", "y" },
        .productions = { &APPLY_X_Y_TO_P },
        .opcode = isa::OpCode::ANONYMOUS_FUNCTION
    }
};

static type::Function FN_Y = type::Function {
    .function_definition = isa::Instruction {
        .literals = { "f" },
        .productions = { &FN_Y_INNER_2 },
        .opcode = isa::OpCode::ANONYMOUS_FUNCTION
    }
};

Result IF = Result {
    .value = shared_ptr<void>(shared_ptr<void>{} , &FN_IF),
    .type_name = type::Function::name()
};

Result Y = Result {
    .value = shared_ptr<void>(shared_ptr<void>{} , &FN_Y),
    .type_name = type::Function::name()
};

}
}
}
}
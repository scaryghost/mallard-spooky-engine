#include "mallard/engine/spooky/exception.hpp"
#include "mallard/engine/spooky/environment.hpp"

#include "mallard/engine/spooky/stdlib/control.hpp"
#include "mallard/engine/spooky/stdlib/boolean.hpp"

#include <cstring>
#include <functional>
#include <sstream>

using namespace std;

namespace mallard {
namespace engine {
namespace spooky {

Environment& Environment::global() {
    static Environment global_env = {
        .memory_pool = nullptr,
        .variables = {
            { "if", stdlib::IF },
            { "true", *stdlib::TRUE },
            { "false", *stdlib::FALSE },
            { "Y", stdlib::Y }
        },
        .parent = nullptr
    };
    return global_env;
}

Environment::~Environment() {
    for(auto& it: variables) {
        it.second.value.reset();
    }
    memory_pool = nullptr;
    parent.reset();
}

void Environment::persist_result(const string& variable_name, const Result& result) {
    variables[variable_name] = result;
}

const Result& Environment::find_variable(const std::string& variable_name) const {
    switch(variables.count(variable_name)) {
        case 0: {
            if (parent != nullptr) {
                return parent->find_variable(variable_name);
            }

            stringstream msg;
            msg << "Undefined variable: " << variable_name;

            throw Exception(msg.str());
        }
        default:
            return variables.at(variable_name);
    }
}

}
}
}

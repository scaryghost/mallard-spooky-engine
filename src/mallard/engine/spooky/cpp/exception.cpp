#include "mallard/engine/spooky/exception.hpp"

using namespace std;

namespace mallard {
namespace engine {
namespace spooky {

Exception::Exception(const std::string& what_arg) : runtime_error(what_arg) {

}

Exception::~Exception() {

}

}
}
}

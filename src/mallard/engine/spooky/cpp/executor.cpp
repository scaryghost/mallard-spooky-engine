#include "mallard/engine/spooky/builtin.hpp"
#include "mallard/engine/spooky/exception.hpp"
#include "mallard/engine/spooky/executor.hpp"
#include "mallard/engine/spooky/type/applied_function.hpp"
#include "mallard/engine/spooky/type/function.hpp"
#include "mallard/engine/spooky/type/integer.hpp"
#include "mallard/engine/spooky/type/list.hpp"

#include <cstdint>
#include <iterator>
#include <sstream>
#include <stdexcept>
#include <unordered_map>
#include <vector>

using namespace std;
using mallard::isa::OpCode;

namespace mallard {
namespace engine {
namespace spooky {

static const unordered_map<OpCode, Result(*)(const shared_ptr<Environment>&, const isa::Instruction&)> INSTRUCTION_EXECUTORS = {
    { OpCode::INT_LITERAL, execute_int_literal },
    { OpCode::BINARY_EXPRESSION, execute_binary_expression },
    { OpCode::ANONYMOUS_FUNCTION, execute_anonymous_function },
    { OpCode::VARIABLE, execute_variable },
    { OpCode::FUNCTION_APPLICATION, execute_function_application },
    { OpCode::LIST_LITERAL, execute_list_literal },
};

Result execute_instruction(const shared_ptr<Environment>& env, const isa::Instruction& instruction) {
    try {
        return INSTRUCTION_EXECUTORS.at(instruction.opcode)(env, instruction);
    } catch (out_of_range&) {
        stringstream msg;

        msg << "No executor defined for instruction opcode: " << static_cast<int32_t>(instruction.opcode);
        throw_with_nested(Exception(msg.str()));
    }
}

Result execute_anonymous_function(const shared_ptr<Environment>& env, const isa::Instruction& instruction) {
    return type::AppliedFunction::create(env, instruction);
}

Result execute_binary_expression(const shared_ptr<Environment>& env, const isa::Instruction& instruction) {
    auto src1 = *(instruction.productions[0]);

    if (instruction.literals[0] == "=") {
        // assignment is a special case where productions[1] is lazy eval

        switch(src1.opcode) {
        case OpCode::VARIABLE:
            env->pending_variables.emplace(
                src1.literals[0], 
                LazyResult {
                    .env = env, 
                    .instruction = *(instruction.productions[1])
                }
            );

            return Result {
                shared_ptr<void>(),
                MT_UNIT
            };
        default:
            if (instruction.literals[0] != "=") {
                stringstream msg;

                msg << "Cannot assign an expression to opcode " << static_cast<int32_t>(src1.opcode);
                throw Exception(msg.str());
            }
        }
    }

    auto left_result = execute_instruction(env, src1);
    auto right_result = execute_instruction(env, *(instruction.productions[1]));
    auto it = BUILTIN_FUNCTIONS.find({
        .parameter_types = { left_result.type_name, right_result.type_name },
        .name = instruction.literals[0]
    });

    if (it == BUILTIN_FUNCTIONS.end()) {
        stringstream msg;

        msg << "No binary operator defined for types: ["
            << left_result.type_name << ", " << right_result.type_name << "]";
        throw Exception(msg.str());
    }

    return (it->second)(env, left_result.value.get(), right_result.value.get());
}

Result execute_variable(const shared_ptr<Environment>& env, const isa::Instruction& instruction) {
    auto variable_name = instruction.literals[0];

    if (env->variables.count(variable_name)) {
        return env->variables.at(variable_name);
    }

    if (env->pending_variables.count(variable_name)) {
        auto it = env->pending_variables.at(variable_name);

        env->variables.insert({ variable_name, execute_instruction(it.env, it.instruction) });
        env->pending_variables.erase(variable_name);
        return env->variables.at(variable_name);
    }

    if (env->parent != nullptr) {
        return execute_variable(env->parent, instruction);
    }

    stringstream msg;
    msg << "Undefined variable: " << variable_name;

    throw Exception(msg.str());
}

Result execute_int_literal(const shared_ptr<Environment>& env, const isa::Instruction& instruction) {    
    return type::Integer::wrap(env->memory_pool, stoi(instruction.literals[0]));
}

Result execute_function_application(const shared_ptr<Environment>& env, const isa::Instruction& instruction) {
    auto top_result = execute_instruction(env, *(instruction.productions[0]));

    if (top_result.type_name == type::Function::name()) {
        auto fn = top_result.as<type::Function>();
        top_result = type::AppliedFunction::create(env, fn->function_definition);
    } else if (top_result.type_name != type::AppliedFunction::name()) {
        stringstream msg;

        msg << "Cannot apply function application to: " << top_result.type_name;
        throw Exception(msg.str());
    }

    vector<isa::Instruction*> parameters = { next(instruction.productions.begin()), instruction.productions.end() };
    auto reduced_fn = top_result.as<type::AppliedFunction>()->bind(env, parameters);
    auto fn = reduced_fn.as<type::AppliedFunction>();

    if (!fn->all_bound()) {
        return reduced_fn;
    }

    size_t instr_k = 0;
    for(instr_k; instr_k < fn->function_definition.productions.size() - 1; instr_k++) {
        execute_instruction(fn->env, *(fn->function_definition.productions[instr_k]));
    }

    return execute_instruction(fn->env, *(fn->function_definition.productions[instr_k]));
}

Result execute_list_literal(const shared_ptr<Environment>& env, const isa::Instruction& instruction) {
    auto memory_pool = env->memory_pool;
    auto addr = memory_pool->allocate(sizeof(type::List));
    auto list = new(addr) type::List(instruction.productions.size());

    for(size_t i = 0; i < instruction.productions.size(); i++) {
        list->pending.insert({ 
            i, 
            LazyResult {
                .env = env, 
                .instruction = *(instruction.productions[i])
            }
        });
    }

    return Result {
        shared_ptr<void>(list, [memory_pool](auto p) {
            static_cast<type::List*>(p)->~List();
            memory_pool->free(p);
        }),
        .type_name = type::List::name()
    };
}

}
}
}
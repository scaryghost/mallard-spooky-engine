#include "mallard/engine/spooky/debug.hpp"

#include <iterator>

using mallard::isa::OpCode;
using namespace std;

namespace mallard {
namespace engine {
namespace spooky {

ostream& operator <<(ostream& os, const isa::Instruction& instruction) {
    switch(instruction.opcode) {
    case OpCode::INT_LITERAL:
    case OpCode::VARIABLE:
        os << instruction.literals[0];
        break;
    case OpCode::BINARY_EXPRESSION:
        os << *(instruction.productions[0]) << " " << instruction.literals[0] << " " << *(instruction.productions[1]);
        break;
    case OpCode::ANONYMOUS_FUNCTION:
        for(auto& it: instruction.literals) {
            os << "\\" << it << " ";
        }
        os << " -> ";

        if (instruction.productions.size() == 1) {
            os << *(instruction.productions[0]);
        } else {
            os << "{\n";
            for(auto& it: instruction.productions) {
                os << "  " << *it << "\n";
            }
            os << "}";
        }
        break;
    case OpCode::FUNCTION_APPLICATION: {
        auto it = instruction.productions.begin();

        os << **(it++) << "(";
        if (instruction.productions.size() > 1) {
            os << **(it++);
            for(it; it != instruction.productions.end(); it++) {
                os << ", " << **it;
            }
        }
        os << ")";
        break;
    }
    case OpCode::LIST_LITERAL:
        os << "[";
        if (instruction.productions.size() > 0) {
            auto it = instruction.productions.begin();

            os << *it;
            for(it; it != instruction.productions.end(); it++) {
                os << ", " << **it;
            }
        }
        os << "]";
        break;
    }

    return os;
}

}
}
}
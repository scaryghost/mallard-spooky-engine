#include "mallard/engine/spooky/builtin.hpp"

#include "mallard/engine/spooky/type/integer.hpp"
#include "mallard/engine/spooky/type/list.hpp"

using namespace std;

namespace mallard {
namespace engine {
namespace spooky {

#define INT_FN_ENTRY(op, fn) {\
    { .parameter_types = { type::Integer::name(), type::Integer::name() }, .name = #op }, \
    type::Integer::fn\
}

const unordered_map<FunctionKey, Result(*)(const shared_ptr<Environment>& ...)> BUILTIN_FUNCTIONS = {
    INT_FN_ENTRY(+, add),
    INT_FN_ENTRY(-, sub),
    INT_FN_ENTRY(*, mul),
    INT_FN_ENTRY(/, div),
    INT_FN_ENTRY(%, mod),
    INT_FN_ENTRY(==, eq),
    INT_FN_ENTRY(!=, neq),
    INT_FN_ENTRY(<, lt),
    INT_FN_ENTRY(<=, lte),
    INT_FN_ENTRY(>, gt),
    INT_FN_ENTRY(>=, gte),
    {
        {.parameter_types = { type::List::name(), type::List::name() }, .name = "+" },
        type::List::concatenate
    }
};

bool operator==(const FunctionKey& left, const FunctionKey& right) {
    auto name_equals = left.name == right.name;
    auto parameters_equals = left.parameter_types == right.parameter_types;

    return left.name == right.name && left.parameter_types == right.parameter_types;
}

}
}
}

namespace std {

// https://xorshift.di.unimi.it/splitmix64.c
static size_t split_mix64(size_t value) {
    value += 0x9e3779b97f4a7c15;
    value = (value ^ (value >> 30)) * 0xbf58476d1ce4e5b9;
    value = (value ^ (value >> 27)) * 0x94d049bb133111eb;
    return value ^ (value >> 31);
};

size_t hash<mallard::engine::spooky::FunctionKey>::operator()(const mallard::engine::spooky::FunctionKey &k) const {
    size_t seed = k.parameter_types.size() + 1;

    auto combine = [&seed](size_t value) {
        // boost's has combine
        seed ^= value + 0x9e3779b9 + (seed << 6) + (seed >> 2);
    };
    

    for(auto& it: k.parameter_types) {
        combine(split_mix64(hash<string>{}(it)));
    }
    combine(split_mix64(hash<string>{}(k.name)));

    return seed;
}

}
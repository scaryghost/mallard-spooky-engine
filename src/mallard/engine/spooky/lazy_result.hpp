#pragma once

#include "mallard/isa/instruction.hpp"

#include <memory>

namespace mallard {
namespace engine {
namespace spooky {

struct Environment;

struct LazyResult {
    const std::shared_ptr<Environment> env;
    const isa::Instruction instruction;
};

}
}
}
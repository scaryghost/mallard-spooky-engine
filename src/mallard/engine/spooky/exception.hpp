#pragma once

#include <string>
#include <stdexcept>

namespace mallard {
namespace engine {
namespace spooky {

struct Exception : public std::runtime_error {
    Exception(const std::string& what_arg);
    virtual ~Exception();
};

}
}
}
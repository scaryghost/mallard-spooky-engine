#pragma once

#include <cstddef>

namespace mallard {
namespace engine {
namespace spooky {

struct MemoryPool {
    virtual void* allocate(size_t n_bytes) = 0;
    virtual void free(void* ptr) = 0;
};

}
}
}
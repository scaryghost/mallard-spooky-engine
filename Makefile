VOLAUVENT_HOME=build-tools/Volauvent
CXXFLAGS := -std=c++14

.PHONY: all clean

all: src-all
clean: src-clean test-clean
test: test-all

include $(VOLAUVENT_HOME)/global_config.mk

include src/rules.mk
include test/rules.mk

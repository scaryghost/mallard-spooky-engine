#include "catch2/catch.hpp"

#include "mallard/engine/spooky/environment.hpp"
#include "mallard/engine/spooky/executor.hpp"
#include "mallard/engine/spooky/type/list.hpp"

#include "fixture.hpp"

#include <array>

using namespace std;
using mallard::engine::spooky::Environment;
using mallard::engine::spooky::execute_binary_expression;
using mallard::engine::spooky::type::List;
using mallard::isa::Instruction;
using mallard::isa::OpCode;

TEST_CASE_METHOD(Fixture, "list concat", "[operators]") {
    auto env = make_shared<Environment>(
        Environment {
            .memory_pool = &memory_pool
        }
    );
    
    array<Instruction, 4> list_1 = {
        Instruction {
            .productions = { &list_1[1], &list_1[2], &list_1[3] },
            .opcode = OpCode::LIST_LITERAL
        },
        {
            .literals = { "0" },
            .opcode = OpCode::INT_LITERAL
        },
        {
            .literals = { "1" },
            .opcode = OpCode::INT_LITERAL
        },
        {
            .literals = { "2" },
            .opcode = OpCode::INT_LITERAL
        }
    };
    array<Instruction, 3> list_2 = {
        Instruction {
            .productions = { &list_2[1], &list_2[2] },
            .opcode = OpCode::LIST_LITERAL
        },
        {
            .literals = { "3" },
            .opcode = OpCode::INT_LITERAL
        },
        {
            .literals = { "4" },
            .opcode = OpCode::INT_LITERAL
        }
    };

    auto result = execute_binary_expression(env, {
        .literals = { "+" },
        .productions = { &list_1[0], &list_2[0] },
        .opcode = OpCode::BINARY_EXPRESSION
    });
    auto list = result.as<List>();

    CHECK(list->elements.size() == 5);
    CHECK(*((*list)[0].as<int32_t>()) == 0);
    CHECK(*((*list)[1].as<int32_t>()) == 1);
    CHECK(*((*list)[2].as<int32_t>()) == 2);
    CHECK(*((*list)[3].as<int32_t>()) == 3);
    CHECK(*((*list)[4].as<int32_t>()) == 4);
}
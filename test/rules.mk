include $(VOLAUVENT_HOME)/application.mk
CATCH2_DIR := deps/Catch2/single_include

TEST_DEPS := $(src_APP_OUTPUT)
TEST_INC_DIRS := $(CATCH2_DIR) $(src_INC_DIRS)
TEST_LIB_DIRS := $(src_OUTPUT_DIR) $(parser_OUTPUT_DIR) $(engine_OUTPUT_DIR)
TEST_STATIC_LIB := mallard_spooky_engine

$(eval $(call define_app_rules,test,mallard_spooky_engine_test,$(TEST_DEPS),$(TEST_INC_DIRS),$(TEST_LIB_DIRS),$(TEST_STATIC_LIB)))

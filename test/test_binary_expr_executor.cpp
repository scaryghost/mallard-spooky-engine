#include "catch2/catch.hpp"

#include "mallard/engine/spooky/environment.hpp"
#include "mallard/engine/spooky/executor.hpp"

#include "generators/int_generator.hpp"

#include "fixture.hpp"

#include <array>
#include <cstdint>
#include <cstdio>
#include <cstring>
#include <limits>

using namespace std;
using Catch::Matchers::Equals;
using mallard::engine::spooky::Environment;
using mallard::engine::spooky::execute_binary_expression;
using mallard::engine::spooky::execute_function_application;
using mallard::isa::Instruction;
using mallard::isa::OpCode;

static const array<int32_t(*)(int32_t, int32_t), 5> BINARY_OPERATOR_FNS = {
    [](int32_t src1, int32_t src2) { return src1 + src2; },
    [](int32_t src1, int32_t src2) { return src1 - src2; },
    [](int32_t src1, int32_t src2) { return src1 * src2; },
    [](int32_t src1, int32_t src2) { return src1 / src2; },
    [](int32_t src1, int32_t src2) { return src1 % src2; }
};
static const array<const char*, 5> BINARY_OPERATOR_SYMBOLS = { "+", "-", "*", "/", "%" };

TEST_CASE_METHOD(Fixture, "binary expression executor", "[executors]") {
    auto env = make_shared<Environment>(
        Environment {
            .memory_pool = &memory_pool
        }
    );
    auto left_value = GENERATE(take(5, arb_int(0, numeric_limits<int32_t>::max())));
    auto right_value = GENERATE(take(5, arb_int(0, numeric_limits<int32_t>::max())));
    auto op_index = GENERATE(take(4, arb_int(0, BINARY_OPERATOR_SYMBOLS.size() - 1)));

    char left_repr[11], right_repr[11];
    snprintf(left_repr, sizeof(left_repr), "%d", left_value);
    snprintf(right_repr, sizeof(right_repr), "%d", right_value);

    array<Instruction, 3> instructions = {
        Instruction {
            .literals = { BINARY_OPERATOR_SYMBOLS[op_index] },
            .productions = { &instructions[1], &instructions[2] }
        },
        {
            .literals = { left_repr },
            .opcode = OpCode::INT_LITERAL
        },
        {
            .literals = { right_repr },
            .opcode = OpCode::INT_LITERAL
        }
    };
    
    auto result = execute_binary_expression(env, instructions[0]);

    CHECK(*result.as<int32_t>() == BINARY_OPERATOR_FNS[op_index](left_value, right_value));
}

static const array<bool(*)(int32_t, int32_t), 6> BOOLEAN_BINARY_OPERATOR_FNS = {
    [](int32_t src1, int32_t src2) { return src1 == src2; },
    [](int32_t src1, int32_t src2) { return src1 != src2; },
    [](int32_t src1, int32_t src2) { return src1 < src2; },
    [](int32_t src1, int32_t src2) { return src1 <= src2; },
    [](int32_t src1, int32_t src2) { return src1 > src2; },
    [](int32_t src1, int32_t src2) { return src1 >= src2; }
};
static const array<const char*, 6> BOOLEAN_BINARY_OPERATOR_SYMBOLS = { "==", "!=", "<", "<=", ">", ">=" };

TEST_CASE_METHOD(Fixture, "boolean binary expression executor", "[executors]") {
    auto env = make_shared<Environment>(
        Environment {
            .memory_pool = &memory_pool
        }
    );
    int32_t bool_to_int[] = { 0, 1 };

    auto left_value = GENERATE(take(5, arb_int(0, numeric_limits<int32_t>::max())));
    auto right_value = GENERATE(take(5, arb_int(0, numeric_limits<int32_t>::max())));
    auto op_index = GENERATE(take(4, arb_int(0, BOOLEAN_BINARY_OPERATOR_SYMBOLS.size() - 1)));

    char left_repr[11], right_repr[11];
    snprintf(left_repr, sizeof(left_repr), "%d", left_value);
    snprintf(right_repr, sizeof(right_repr), "%d", right_value);

    array<Instruction, 6> instructions = {
        Instruction {
            .productions = { &instructions[1], &instructions[4], &instructions[5] },
            .opcode = OpCode::FUNCTION_APPLICATION
        },
        {
            .literals = { BOOLEAN_BINARY_OPERATOR_SYMBOLS[op_index] },
            .productions = { &instructions[2], &instructions[3] },
            .opcode = OpCode::BINARY_EXPRESSION
        },
        {
            .literals = { left_repr },
            .opcode = OpCode::INT_LITERAL
        },
        {
            .literals = { right_repr },
            .opcode = OpCode::INT_LITERAL
        },
        {
            .literals = { "1" },
            .opcode = OpCode::INT_LITERAL
        },
        {
            .literals = { "0" },
            .opcode = OpCode::INT_LITERAL
        }
    };
    
    auto result = execute_function_application(env, instructions[0]);

    CHECK(*result.as<int32_t>() == bool_to_int[BOOLEAN_BINARY_OPERATOR_FNS[op_index](left_value, right_value)]);
}

TEST_CASE_METHOD(Fixture, "missing binary operator has specific exception message", "[executors]") {
    auto env = make_shared<Environment>(
        Environment {
            .memory_pool = &memory_pool
        }
    );

    array<Instruction, 6> instructions = {
        Instruction {
            .literals = { "-" },
            .productions = { &instructions[1], &instructions[2] },
            .opcode = OpCode::BINARY_EXPRESSION
        },
        {
            .literals = { "x" },
            .productions = { &instructions[3] },
            .opcode = OpCode::ANONYMOUS_FUNCTION
        },
        {
            .literals = { "5" },
            .opcode = OpCode::INT_LITERAL
        },
        {
            .literals = { "x" },
            .opcode = OpCode::VARIABLE
        },
    };
    
    CHECK_THROWS_WITH(
        execute_binary_expression(env, instructions[0]), 
        Equals("No binary operator defined for types: [AppliedFunction, i32]")
    );
}
#include "catch2/catch.hpp"

#include "mallard/engine/spooky/environment.hpp"
#include "mallard/engine/spooky/executor.hpp"
#include "mallard/engine/spooky/type/list.hpp"

#include "generators/int_generator.hpp"

#include "fixture.hpp"

#include <array>
#include <cstdint>
#include <cstdio>
#include <limits>

using namespace std;
using mallard::engine::spooky::Environment;
using mallard::engine::spooky::execute_int_literal;
using mallard::engine::spooky::execute_list_literal;
using mallard::engine::spooky::type::List;
using mallard::isa::Instruction;
using mallard::isa::OpCode;

TEST_CASE_METHOD(Fixture, "int literal executor", "[literal executors]") {
    auto env = make_shared<Environment>(
        Environment {
            .memory_pool = &memory_pool
        }
    );
    auto int_value = GENERATE(take(25, arb_int(0, numeric_limits<int32_t>::max())));

    char str_repr[11];
    snprintf(str_repr, sizeof(str_repr), "%d", int_value);

    auto result = execute_int_literal(
        env,
        { .literals = { str_repr }, .opcode = OpCode::INT_LITERAL }
    );

    SECTION("result type is MT_INT") {
        CHECK(result.type == MT_INT);
    }

    SECTION("str to int conversion is correct") {
        CHECK(*result.as<int32_t>() == int_value);
    }
}

TEST_CASE_METHOD(Fixture, "list literal executor", "[literal executors]") {
    auto env = make_shared<Environment>(
        Environment {
            .memory_pool = &memory_pool
        }
    );
    
    array<Instruction, 6> instructions = {
        Instruction {
            .productions = { &instructions[1], &instructions[2], &instructions[3], &instructions[4], &instructions[5] },
            .opcode = OpCode::LIST_LITERAL
        },
        {
            .literals = { "1234" },
            .opcode = OpCode::INT_LITERAL
        },
        {
            .literals = { "314159" },
            .opcode = OpCode::INT_LITERAL
        },
        {
            .literals = { "24089" },
            .opcode = OpCode::INT_LITERAL
        },
        {
            .literals = { "928347" },
            .opcode = OpCode::INT_LITERAL
        },
        {
            .literals = { "87903142" },
            .opcode = OpCode::INT_LITERAL
        }
    };

    auto result = execute_list_literal(env, instructions[0]);
    auto list = result.as<List>();

    CHECK(list->elements.size() == 5);
    CHECK(*((*list)[0].as<int32_t>()) == 1234);
    CHECK(*((*list)[1].as<int32_t>()) == 314159);
    CHECK(*((*list)[2].as<int32_t>()) == 24089);
    CHECK(*((*list)[3].as<int32_t>()) == 928347);
    CHECK(*((*list)[4].as<int32_t>()) == 87903142);
}
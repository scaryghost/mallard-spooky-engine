#include "generators/int_generator.hpp"

using namespace std;
using namespace Catch::Generators;

IntGenerator::IntGenerator(int32_t min, int32_t max) : 
    engine(random_device()),
    distribution(min, max),
    current(distribution(engine))
{

}

int32_t const& IntGenerator::get() const {
    return current;
}

bool IntGenerator::next() {
    current = distribution(engine);
    return true;
}


 GeneratorWrapper<int32_t> arb_int(int32_t min, int32_t max) {
    return GeneratorWrapper<std::int32_t>(
        pf::make_unique<IntGenerator>(min, max)
    );
}
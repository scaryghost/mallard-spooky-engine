#include "catch2/catch.hpp"

#include <random>
#include <cstdint>

/**
 * Genrates random integers between [min] and [max].
 * Unlike the Catch2 provided [RandomIntegerGenerator], this generator will provide 
 * unique integers for every run.
 */
struct IntGenerator final : public Catch::Generators::IGenerator<std::int32_t> {
    IntGenerator(std::int32_t min, std::int32_t max);

    virtual std::int32_t const& get() const;
    virtual bool next();

private:
    std::random_device random_device;
    std::mt19937 engine;
    std::uniform_int_distribution<> distribution;
    std::int32_t current;
};

Catch::Generators::GeneratorWrapper<std::int32_t> arb_int(std::int32_t min, std::int32_t max);
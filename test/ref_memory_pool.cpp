#include "ref_memory_pool.hpp"

#include <cstdlib>

void* RefMemoryPool::allocate(size_t n_bytes) {
    return std::malloc(n_bytes);
}

void RefMemoryPool::free(void* ptr) {
    std::free(ptr);
}
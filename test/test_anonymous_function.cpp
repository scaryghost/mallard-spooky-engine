#include "catch2/catch.hpp"

#include "mallard/engine/spooky/environment.hpp"
#include "mallard/engine/spooky/executor.hpp"
#include "mallard/engine/spooky/type/function.hpp"
#include "generators/int_generator.hpp"

#include "fixture.hpp"

#include <array>
#include <cstdint>
#include <cstdio>
#include <limits>

using namespace std;
using mallard::engine::spooky::Environment;
using mallard::engine::spooky::execute_instruction;
using mallard::engine::spooky::execute_function_application;
using mallard::engine::spooky::type::Function;
using mallard::isa::Instruction;
using mallard::isa::OpCode;

auto var_add = Instruction {
    .literals = { "add" },
    .opcode = OpCode::VARIABLE
};

auto var_add3 = Instruction {
    .literals = { "add3" },
    .opcode = OpCode::VARIABLE
};

array<Instruction, 9> setup_instructions = {
    Instruction {
        .literals = { "=" },
        .productions = { &var_add, &setup_instructions[2] },
        .opcode = OpCode::BINARY_EXPRESSION
    },
    {
        .literals = { "=" },
        .productions = { &var_add3, &setup_instructions[4] },
        .opcode = OpCode::BINARY_EXPRESSION
    },
    {
        .literals = { "x", "y" },
        .productions = { &setup_instructions[3] },
        .opcode = OpCode::ANONYMOUS_FUNCTION
    },
    {
        .literals = { "+" },
        .productions = { &setup_instructions[6], &setup_instructions[7] },
        .opcode = OpCode::BINARY_EXPRESSION
    },
    {
        .productions = { &setup_instructions[5], &setup_instructions[8] },
        .opcode = OpCode::FUNCTION_APPLICATION
    },
    {
        .literals = { "add" },
        .opcode = OpCode::VARIABLE
    },
    {
        .literals = { "x" },
        .opcode = OpCode::VARIABLE
    },
    {
        .literals = { "y" },
        .opcode = OpCode::VARIABLE
    },
    {
        .literals = { "3" },
        .opcode = OpCode::INT_LITERAL
    }
};

TEST_CASE_METHOD(Fixture, "function binding", "[anonymous function]") {
    auto env = make_shared<Environment>(
        Environment {
            .memory_pool = &memory_pool
        }
    );
    execute_instruction(env, setup_instructions[0]);
    execute_instruction(env, setup_instructions[1]);
    
    auto int_value = GENERATE(take(25, arb_int(0, numeric_limits<int32_t>::max())));

    char str_repr[11];
    snprintf(str_repr, sizeof(str_repr), "%d", int_value);

    array<Instruction, 3> test_instructions = {
        Instruction {
            .productions = { &test_instructions[1], &test_instructions[2] },
            .opcode = OpCode::FUNCTION_APPLICATION
        },
        {
            .literals = { "add3" },
            .opcode = OpCode::VARIABLE
        },
        {
            .literals = { str_repr },
            .opcode = OpCode::INT_LITERAL
        }
    };
    auto result = execute_function_application(env, test_instructions[0]);

    CHECK(*result.as<int32_t>() == int_value + 3);
}


auto var_sub = Instruction {
    .literals = { "sub" },
    .opcode = OpCode::VARIABLE
};

auto var_sub7 = Instruction {
    .literals = { "sub7" },
    .opcode = OpCode::VARIABLE
};

TEST_CASE_METHOD(Fixture, "curried function", "[anonymous function]") {
    array<Instruction, 10> setup = {
        Instruction {
            .literals = { "=" },
            .productions = { &var_sub, &setup[2] },
            .opcode = OpCode::BINARY_EXPRESSION
        },
        {
            .literals = { "=" },
            .productions = { &var_sub7, &setup[5] },
            .opcode = OpCode::BINARY_EXPRESSION
        },
        {
            .literals = { "x" },
            .productions = { &setup[3] },
            .opcode = OpCode::ANONYMOUS_FUNCTION
        },
        {
            .literals = { "y" },
            .productions = { &setup[4] },
            .opcode = OpCode::ANONYMOUS_FUNCTION
        },
        {
            .literals = { "-" },
            .productions = { &setup[7], &setup[8] },
            .opcode = OpCode::BINARY_EXPRESSION
        },
        {
            .productions = { &setup[6], &setup[9] },
            .opcode = OpCode::FUNCTION_APPLICATION
        },
        {
            .literals = { "sub" },
            .opcode = OpCode::VARIABLE
        },
        {
            .literals = { "x" },
            .opcode = OpCode::VARIABLE
        },
        {
            .literals = { "y" },
            .opcode = OpCode::VARIABLE
        },
        {
            .literals = { "7" },
            .opcode = OpCode::INT_LITERAL
        }
    };

    auto env = make_shared<Environment>(
        Environment {
            .memory_pool = &memory_pool
        }
    );
    execute_instruction(env, setup[0]);
    execute_instruction(env, setup[1]);
    
    auto int_value = GENERATE(take(25, arb_int(0, numeric_limits<int32_t>::max())));

    char str_repr[11];
    snprintf(str_repr, sizeof(str_repr), "%d", int_value);

    array<Instruction, 3> test_instructions = {
        Instruction {
            .productions = { &test_instructions[1], &test_instructions[2] },
            .opcode = OpCode::FUNCTION_APPLICATION
        },
        {
            .literals = { "sub7" },
            .opcode = OpCode::VARIABLE
        },
        {
            .literals = { str_repr },
            .opcode = OpCode::INT_LITERAL
        }
    };
    auto result = execute_function_application(env, test_instructions[0]);

    CHECK(*result.as<int32_t>() == 7 - int_value);
}
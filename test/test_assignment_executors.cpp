#include "catch2/catch.hpp"

#include "mallard/engine/spooky/environment.hpp"
#include "mallard/engine/spooky/executor.hpp"

#include "generators/int_generator.hpp"

#include "fixture.hpp"

#include <array>
#include <cstdint>
#include <cstdio>
#include <limits>

using namespace std;
using mallard::engine::spooky::Environment;
using mallard::engine::spooky::execute_instruction;
using mallard::engine::spooky::execute_variable;
using mallard::isa::Instruction;
using mallard::isa::OpCode;

auto var_x = Instruction {
    .literals = { "x" },
    .opcode = OpCode::VARIABLE
};

TEST_CASE_METHOD(Fixture, "assignment executor", "[executors]") {
    auto env = make_shared<Environment>(
        Environment {
            .memory_pool = &memory_pool
        }
    );
    auto int_value = GENERATE(take(25, arb_int(0, numeric_limits<int32_t>::max())));

    char str_repr[11];
    snprintf(str_repr, sizeof(str_repr), "%d", int_value);

    array<Instruction, 2> instructions = {
        Instruction {
            .literals = { "=" },
            .productions = { &var_x, &instructions[1] },
            .opcode = OpCode::BINARY_EXPRESSION
        },
        {
            .literals = { str_repr },
            .opcode = OpCode::INT_LITERAL
        }
    };
    auto assignment_result = execute_instruction(env, instructions[0]);
    auto variable_result = execute_variable(env, { .literals = { "x" }, .opcode = OpCode::VARIABLE });

    CHECK(assignment_result.type == MT_UNIT);
    CHECK(*variable_result.as<int32_t>() == int_value);
}

TEST_CASE_METHOD(Fixture, "assignment is lazy", "[executors]") {
    auto env = make_shared<Environment>(
        Environment {
            .memory_pool = &memory_pool
        }
    );

    auto int_value = GENERATE(take(25, arb_int(0, numeric_limits<int32_t>::max())));

    char str_repr[11];
    snprintf(str_repr, sizeof(str_repr), "%d", int_value);

    array<Instruction, 4> instructions = {
        Instruction {
            .literals = { "=" },
            .productions = { &var_x, &instructions[1] },
            .opcode = OpCode::BINARY_EXPRESSION
        },
        {
            .literals = { "/" },
            .productions = { &instructions[2], &instructions[3] },
            .opcode = OpCode::BINARY_EXPRESSION
        },
        {
            .literals = { str_repr },
            .opcode = OpCode::INT_LITERAL
        },
        {
            .literals = { "0" },
            .opcode = OpCode::INT_LITERAL
        }
    };
    
    CHECK_NOTHROW(execute_instruction(env, instructions[0]));
}
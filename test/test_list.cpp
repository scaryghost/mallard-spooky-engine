#include "catch2/catch.hpp"

#include "mallard/engine/spooky/environment.hpp"
#include "mallard/engine/spooky/executor.hpp"
#include "mallard/engine/spooky/type/list.hpp"

#include "generators/int_generator.hpp"

#include "fixture.hpp"

#include <array>
#include <cstdint>
#include <cstdio>
#include <limits>

using namespace std;
using mallard::engine::spooky::Environment;
using mallard::engine::spooky::execute_list_literal;
using mallard::engine::spooky::type::List;
using mallard::isa::Instruction;
using mallard::isa::OpCode;

TEST_CASE_METHOD(Fixture, "lists are lazy", "[list]") {
    auto env = make_shared<Environment>(
        Environment {
            .memory_pool = &memory_pool
        }
    );

    array<Instruction, 6> faulty_instr = {
        Instruction {
            .literals = { "/" },
            .productions = { &faulty_instr[1], &faulty_instr[2] },
            .opcode = OpCode::BINARY_EXPRESSION
        },
        {
            .literals = { "1" },
            .opcode = OpCode::INT_LITERAL
        },
        {
            .literals = { "0" },
            .opcode = OpCode::INT_LITERAL
        },
    };
    
    array<Instruction, 6> instructions = {
        Instruction {
            .productions = { &faulty_instr[0] },
            .opcode = OpCode::LIST_LITERAL
        }
    };

    CHECK_NOTHROW(execute_list_literal(env, instructions[0]));
}
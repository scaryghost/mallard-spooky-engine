#include "catch2/catch.hpp"

#include "mallard/engine/spooky/environment.hpp"
#include "mallard/engine/spooky/executor.hpp"
#include "mallard/engine/spooky/type/function.hpp"

#include "fixture.hpp"

#include <array>
#include <cstdint>
#include <cstdio>
#include <limits>

#include <iostream>

using namespace std;
using mallard::engine::spooky::Environment;
using mallard::engine::spooky::execute_instruction;

using mallard::isa::Instruction;
using mallard::isa::OpCode;

TEST_CASE_METHOD(Fixture, "function application is lazy", "[function application]") {
    auto env = make_shared<Environment>(
        Environment {
            .memory_pool = &memory_pool,
            .parent = shared_ptr<Environment>(shared_ptr<Environment>{} , &Environment::global())
        }
    );
    
    array<Instruction, 7> instructions = {
        Instruction {
            .productions = { &instructions[1], &instructions[2], &instructions[3], &instructions[4] },
            .opcode = OpCode::FUNCTION_APPLICATION
        },
        {
            .literals = { "if" },
            .opcode = OpCode::VARIABLE
        },
        {
            .literals = { "true" },
            .opcode = OpCode::VARIABLE
        },
        {
            .literals = { "12345" },
            .opcode = OpCode::INT_LITERAL
        },
        {
            .literals = { "/" },
            .productions = { &instructions[5], &instructions[6] },
            .opcode = OpCode::BINARY_EXPRESSION
        },
        {
            .literals = { "1" },
            .opcode = OpCode::INT_LITERAL
        },
        {
            .literals = { "0" },
            .opcode = OpCode::INT_LITERAL
        }
    };
    auto result = execute_function_application(env, instructions[0]);

    CHECK(*result.as<int32_t>() == 12345);
}

/*
    val diff = \f \n -> if(n == 0, 0, n - f(n - 1))
    Y(sum)(10)"
 */
TEST_CASE_METHOD(Fixture, "Y combinator", "[function application]") {
    auto env = make_shared<Environment>(
        Environment {
            .memory_pool = &memory_pool,
            .parent = shared_ptr<Environment>(shared_ptr<Environment>{} , &Environment::global())
        }
    );

    auto var_diff = Instruction {
        .literals = { "diff" },
        .opcode = OpCode::VARIABLE
    };
    
    array<Instruction, 20> sum_definition = {
        Instruction {
            .literals = { "f", "n" },
            .productions = { &sum_definition[1] },
            .opcode = OpCode::ANONYMOUS_FUNCTION
        },
        {
            .productions = { &sum_definition[8], &sum_definition[2], &sum_definition[7], &sum_definition[3] },
            .opcode = OpCode::FUNCTION_APPLICATION
        },
        // n == 0
        {
            .literals = { "==" },
            .productions = { &sum_definition[9], &sum_definition[7] },
            .opcode = OpCode::BINARY_EXPRESSION
        },
        // n - f(n - 1)
        {
            .literals = { "-" },
            .productions = { &sum_definition[9], &sum_definition[4] },
            .opcode = OpCode::BINARY_EXPRESSION
        },
        // f(n - 1)
        {
            .productions = { &sum_definition[10], &sum_definition[5] },
            .opcode = OpCode::FUNCTION_APPLICATION
        },
        // n - 1
        {
            .literals = { "-" },
            .productions = { &sum_definition[9], &sum_definition[6] },
            .opcode = OpCode::BINARY_EXPRESSION
        },
        {
            .literals = { "1" },
            .opcode = OpCode::INT_LITERAL
        },
        {
            .literals = { "0" },
            .opcode = OpCode::INT_LITERAL
        },
        {
            .literals = { "if" },
            .opcode = OpCode::VARIABLE
        },
        {
            .literals = { "n" },
            .opcode = OpCode::VARIABLE
        },
        {
            .literals = { "f" },
            .opcode = OpCode::VARIABLE
        }
    };
    array<Instruction, 6> instructions = {
        Instruction {
            .literals = { "=" },
            .productions = { &var_diff, &sum_definition[0] },
            .opcode = OpCode::BINARY_EXPRESSION
        },
        {
            .productions = { &instructions[3], &instructions[4] },
            .opcode = OpCode::FUNCTION_APPLICATION
        },
        {
            .productions = { &instructions[1], &instructions[5] },
            .opcode = OpCode::FUNCTION_APPLICATION
        },
        {
            .literals = { "Y" },
            .opcode = OpCode::VARIABLE
        },
        {
            .literals = { "diff" },
            .opcode = OpCode::VARIABLE
        },
        {
            .literals = { "10" },
            .opcode = OpCode::INT_LITERAL
        }
    };

    execute_instruction(env, instructions[0]);
    auto result = execute_function_application(env, instructions[2]);

    CHECK(*result.as<int32_t>() == 5);
}
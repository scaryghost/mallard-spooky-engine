#pragma once

#include "mallard/engine/spooky/memory_pool.hpp"

struct RefMemoryPool : public mallard::engine::spooky::MemoryPool {
    virtual void* allocate(size_t n_bytes);
    virtual void free(void* ptr);
};